<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Language;
use App\Models\Post;
use App\Models\Settings;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\View
    {
        return view('backend.categories.index')->with('categories', Category::all())->with('languages', Language::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the category fields
        $request->validate([
            'name' => 'required|max:255|min:2',
            'slug' => 'required|unique:categories|alpha_dash',
            'lang' => 'required',
        ]);

        $category = new Category();
        $category->name = $request->input('name');
        $category->lang = $request->input('lang');
        $category->slug = SlugService::createSlug(Category::class, 'slug', $request->input('name'));
        $category->save();

        $request->session()->flash('success', 'Category Created Successfuly');

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $category = Category::where('slug', $category)->first();

        if ($category['lang'] == LaravelLocalization::getCurrentLocale()) {
            $title = translate('Default Title', 'seo');
            $description = translate('Default Description', 'seo');
            $keyword = translate('Default keywords', 'seo');
            $canonical = url()->current();
            SEOMeta::setTitle($title.' '.Settings::selectSettings('separator').' '.$category->name);
            SEOMeta::setDescription($description);
            SEOMeta::setKeywords($keyword);
            SEOMeta::setCanonical($canonical);
            OpenGraph::setTitle($title.' '.Settings::selectSettings('separator').' '.$category->name);
            OpenGraph::setDescription($description);
            OpenGraph::setSiteName(Settings::selectSettings('name'));
            OpenGraph::addImage(asset(Settings::selectSettings('og_image')));
            OpenGraph::setUrl($canonical);
            OpenGraph::addProperty('type', 'article');

            $limit = Settings::selectSettings('max_posts');
            $posts = Post::where('status', '=', 1)->where('category_id', '=', $category->id)->latest()->paginate($limit);

            return view('frontend.category', [
                'posts' => $posts,
                'category' => $category,
            ]);
        }

        return redirect()->route('home');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category): \Illuminate\Contracts\View\View
    {
        return view('backend.categories.edit')->with('category', $category)->with('languages', Language::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required|max:255|min:2',
            'slug' => 'required|unique:categories,slug,'.$category->id,
            'lang' => 'required',
        ]);

        $category->update([
            'name' => $request->input('name'),
            'lang' => $request->input('lang'),
            'slug' => SlugService::createSlug(Category::class, 'slug', $request->input('slug'), ['unique' => false]),
        ]);

        $request->session()->flash('success', 'Category Updated Successfuly');

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {
        $category->delete();

        $request->session()->flash('success', 'Category Deleted Successfuly');

        return redirect()->route('categories.index');
    }

    public function checkSlug(Request $request): \Illuminate\Http\JsonResponse
    {
        $slug = SlugService::createSlug(Category::class, 'slug', $request->input('name'));

        return response()->json(['slug' => $slug]);
    }
}
