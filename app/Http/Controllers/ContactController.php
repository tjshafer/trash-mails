<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use App\Models\Settings;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    // Create Contact Form
    public function index(Request $request)
    {
        $title = translate('Contact Page Title', 'seo');
        $description = translate('Contact Page Description', 'seo');
        $keyword = translate('Contact Page keywords', 'seo');
        $canonical = url()->current();
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::setKeywords($keyword);
        SEOMeta::setCanonical($canonical);
        OpenGraph::setTitle($title);
        OpenGraph::setDescription($description);
        OpenGraph::setSiteName(Settings::selectSettings('name'));
        OpenGraph::addImage(asset(Settings::selectSettings('og_image')));
        OpenGraph::setUrl($canonical);
        OpenGraph::addProperty('type', 'article');

        return view('frontend.contact');
    }

    // Send mail to admin
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        // Form validation

        if (empty(config('settings.recaptcha_secret_key'))) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'subject' => 'required',
                'message' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'subject' => 'required',
                'message' => 'required',
                'g-recaptcha-response' => 'required|captcha',
            ]);
        }

        if (! config('settings.demo_mode')) {
            Mail::to(Settings::selectSettings('MAIL_TO_ADDRESS'))->send(new ContactUs($request));
        }

        return redirect()->back()->with('success', translate('We have received your message and would like to thank you for writing to us.'));
    }
}
