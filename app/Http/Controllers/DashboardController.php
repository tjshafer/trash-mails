<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Post;
use App\Models\Statistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class DashboardController extends Controller
{
    public function index()
    {
        $total_email = Statistic::where('key', 'total_email_pay_day')
            ->latest()->limit(7)->get();

        $total_email = $total_email->reverse();

        $total_messges = Statistic::where('key', 'total_messges_pay_day')
            ->latest()->limit(7)->get();

        $total_messges = $total_messges->reverse();

        $posts = Post::all()->count();
        $pages = Page::all()->count();

        return view('backend.dashboard', [
            'posts' => $posts,
            'pages' => $pages,
            'total_email' => $total_email,
            'total_messges' => $total_messges,
        ]);
    }

    public function settings()
    {
        return view('backend.settings.index');
    }

    public function clear(Request $request): \Illuminate\Http\RedirectResponse
    {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
        Artisan::call('optimize:clear');
        $request->session()->flash('success', 'Cache Cleared Successfuly');

        return redirect()->back();
    }
}
