<?php

namespace App\Http\Controllers;

use App\Models\Feature;
use App\Models\Language;
use Illuminate\Http\Request;
use voku\helper\AntiXSS;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\View
    {
        return view('backend.features.index')->with('features', Feature::all())->with('languages', Language::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the Feature fields
        $request->validate([
            'icon' => 'required|max:255',
            'title' => 'required|max:255',
            'description' => 'required|max:300',
            'lang' => 'required',
        ]);

        $antiXss = new AntiXSS();

        $request_icon = strip_tags($request->input('icon'), '<i>');

        $icon = $antiXss->xss_clean($request_icon);

        $feature = new Feature();
        $feature->icon = $icon;
        $feature->title = $request->input('title');
        $feature->description = $request->input('description');
        $feature->lang = $request->input('lang');
        $feature->save();

        $request->session()->flash('success', 'Feature Created Successfuly');

        return redirect()->route('features.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Feature
     */
    public function edit(Feature $feature): \Illuminate\Contracts\View\View
    {
        return view('backend.features.edit')->with('feature', $feature)->with('languages', Language::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Feature  $Feature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feature $feature)
    {
        $request->validate([
            'icon' => 'required|max:255',
            'title' => 'required|max:255',
            'description' => 'required|max:300',
            'lang' => 'required',
        ]);

        $antiXss = new AntiXSS();

        $request_icon = strip_tags($request->input('icon'), '<i>');

        $icon = $antiXss->xss_clean($request_icon);

        $feature->update([
            'icon' => $icon,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'lang' => $request->input('lang'),
        ]);

        $request->session()->flash('success', 'Feature Updated Successfuly');

        return redirect()->route('features.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Feature $feature)
    {
        $feature->delete();

        $request->session()->flash('success', 'Feature Deleted Successfuly');

        return redirect()->route('features.index');
    }
}
