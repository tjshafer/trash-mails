<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use voku\helper\AntiXSS;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\View
    {
        return view('backend.menu.index')->with('links', Menu::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the Menu fields

        $request->validate([
            'icon' => 'required_without:title|max:255',
            'title' => 'required_without:icon|max:255',
            'url' => 'required|max:255|url',
            'postion' => 'boolean',
            'target' => 'boolean',
        ]);

        if ($request->input('target') == null) {
            $request->target = 0;
        }

        $antiXss = new AntiXSS();

        $request_icon = strip_tags($request->input('icon'), '<i>');

        $icon = $antiXss->xss_clean($request_icon);

        $menu = new Menu();
        $menu->icon = $icon;
        $menu->title = $request->input('title');
        $menu->url = $request->input('url');
        $menu->postion = $request->input('postion');
        $menu->target = $request->input('target');
        $menu->save();

        $request->session()->flash('success', 'Link Created Successfuly');

        return redirect()->route('menu.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Menu
     */
    public function edit(Menu $menu): \Illuminate\Contracts\View\View
    {
        return view('backend.menu.edit')->with('menu', $menu);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $request->validate([
            'icon' => 'required_without:title|max:255',
            'title' => 'required_without:icon|max:255',
            'url' => 'required|max:255|url',
            'postion' => 'boolean',
            'target' => 'boolean',
        ]);

        if ($request->input('target') == null) {
            $request->target = 0;
        }

        $antiXss = new AntiXSS();

        $request_icon = strip_tags($request->input('icon'), '<i>');

        $icon = $antiXss->xss_clean($request_icon);

        $menu->update([
            'icon' => $icon,
            'title' => $request->input('title'),
            'url' => $request->input('url'),
            'postion' => $request->input('postion'),
            'target' => $request->input('target'),
        ]);

        $request->session()->flash('success', 'Link Updated Successfuly');

        return redirect()->route('menu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Menu $menu)
    {
        $menu->delete();

        $request->session()->flash('success', 'Link Deleted Successfuly');

        return redirect()->route('menu.index');
    }
}
