<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        return view('backend.settings.profile', ['user' => $user]);
    }

    public function changeInfo(Request $request): \Illuminate\Http\RedirectResponse
    {
        $user = $request->user();

        $request->validate([
            'photo' => 'mimes:jpeg,png,jpg|max:2048',
            'name' => 'required|string|min:4',
            'email' => 'required|unique:users,email,'.$user->id,
        ]);

        if ($request->has('photo')) {
            $file = $request->photo;
            $user->avater = FileUpload($file, 'uploads/', $user->avater);
        }

        $user->update([
            $user->name = $request->name,
            $user->email = $request->email,
            $user->avater = $user->avater,
        ]);

        return redirect()->back()->with('success', 'Profile successfully changed!');
    }

    public function changePassword(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);

        $user = $request->user();

        if (! Hash::check($request->current_password, $user->password)) {
            return redirect()->back()->with('error', 'Current password does not match!');
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->back()->with('success', 'Password successfully changed!');
    }
}
