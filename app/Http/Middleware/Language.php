<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Cookie::has('locale')) {
            $locale = Cookie::get('locale');
        } else {
            $locale = config('settings.default_language');
        }

        App::setLocale($locale);
        Cookie::queue('locale', $locale, 365 * 1440);

        return $next($request);
    }
}
