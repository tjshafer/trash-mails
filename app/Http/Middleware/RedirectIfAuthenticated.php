<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $role = $request->user()->role;

            switch ($role) {
                case 'admin':
                    return redirect()->to('/admin/dashboard');
                    break;
                case 'user':
                    return redirect()->to('/');
                    break;

                default:
                    return redirect()->to('/');
                    break;
            }
        }

        return $next($request);
    }
}
