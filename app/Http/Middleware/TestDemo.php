<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TestDemo
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $method = $request->method();

        if ($method == 'POST' or $method == 'PUT' or $method == 'DELETE') {
            $route = Request()->route()->getName();

            if ($route == 'check.imap') {
                return $next($request);
            }

            return redirect()->back()->with('demo', 'Demo version some features are disabled');
        }

        return $next($request);
    }
}
