<?php

namespace App\Models;

use App\Observers\ChangeLangObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code', 'rtl'];

    protected static function boot(): void
    {
        parent::boot();
        Language::observe(ChangeLangObserver::class);
    }
}
