<?php

namespace App\Observers;

use App\Models\Category;
use App\Models\Feature;
use App\Models\Language;
use App\Models\Page;
use App\Models\Post;

class ChangeLangObserver
{
    /**
     * Handle the Language "created" event.
     */
    public function created(Language $language): void
    {
        //
    }

    /**
     * Handle the Language "updated" event.
     */
    public function updated(Language $language): void
    {
        if ($language->wasChanged('code')) {
            $old_lang = $language->getOriginal('code');
            $new_lang = $language->code;

            $features = Feature::where('lang', $old_lang)->get();
            foreach ($features as $feature) {
                $feature->update([
                    'lang' => $new_lang,
                ]);
            }

            $posts = Post::where('lang', $old_lang)->get();
            foreach ($posts as $post) {
                $post->update([
                    'lang' => $new_lang,
                ]);
            }

            $pages = Page::where('lang', $old_lang)->get();
            foreach ($pages as $page) {
                $page->update([
                    'lang' => $new_lang,
                ]);
            }

            $categories = Category::where('lang', $old_lang)->get();
            foreach ($categories as $category) {
                $category->update([
                    'lang' => $new_lang,
                ]);
            }
        }
    }

    /**
     * Handle the Language "deleted" event.
     */
    public function deleted(Language $language): void
    {
        $old_lang = $language->code;

        $features = Feature::where('lang', $old_lang)->get();
        foreach ($features as $feature) {
            $feature->delete();
        }

        $posts = Post::where('lang', $old_lang)->get();
        foreach ($posts as $post) {
            $post->delete();
        }

        $pages = Page::where('lang', $old_lang)->get();
        foreach ($pages as $page) {
            $page->delete();
        }

        $categories = Category::where('lang', $old_lang)->get();
        foreach ($categories as $category) {
            $category->delete();
        }
    }

    /**
     * Handle the Language "restored" event.
     */
    public function restored(Language $language): void
    {
        //
    }

    /**
     * Handle the Language "force deleted" event.
     */
    public function forceDeleted(Language $language): void
    {
    }
}
