<?php

return [
    '$key' => env($key),
    'default_language' => env('DEFAULT_LANGUAGE', 'en'),
    'demo_mode' => env('DEMO_MODE'),
    'https_force' => env('HTTPS_FORCE'),
    'recaptcha_secret_key' => env('RECAPTCHA_SECRET_KEY'),
];
