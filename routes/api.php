<?php

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('domains/{key}', [APIController::class, 'domains']);
Route::get('messages/{email}/{key}', [APIController::class, 'messages']);
Route::get('message/{id}/{key}', [APIController::class, 'message']);
Route::post('email/create/{key}', [APIController::class, 'email_create']);
Route::post('email/delete/{email}/{key}', [APIController::class, 'email_delete']);
Route::post('email/change/{email}/{username}/{domain}/{key}', [APIController::class, 'email_change']);
Route::post('message/delete/{id}/{key}', [APIController::class, 'message_delete']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
