<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;
use App\Models\Settings as ModelSettings;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FeatureController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TrashMailController;
use App\Http\Controllers\Settings\AdsController;
use App\Http\Controllers\Settings\SeoController;
use App\Http\Controllers\Settings\SmtpController;
use App\Http\Controllers\Settings\GeneralController;
use App\Http\Controllers\Settings\ProfileController;
use App\Http\Controllers\Settings\BlogSettingController;

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/update', [DashboardController::class, 'update'])->name('update');

    Route::get('/', function () {
        return to_route('dashboard');
    })->name('admin');

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/settings', [DashboardController::class, 'settings'])->name('settings');

    // General Settings
    Route::get('/settings/general', [GeneralController::class, 'index'])->name('settings.general');
    Route::post('/settings/general/update', [GeneralController::class, 'update'])->name('settings.general.update');
    Route::post('/settings/general/update2', [GeneralController::class, 'update2'])->name('settings.general.update2');
    Route::post('/settings/check/imap', [GeneralController::class, 'check_imap'])->name('check.imap');

    // Seo Settings
    Route::get('/settings/seo', [SeoController::class, 'index'])->name('settings.seo');
    Route::post('/settings/seo/update', [SeoController::class, 'update'])->name('settings.seo.update');

    Route::get('/settings/license', [GeneralController::class, 'license'])->name('settings.license');
    Route::post('/settings/license/update', [GeneralController::class, 'license_update'])->name('settings.license.update');

    // Ads Settings
    Route::get('/settings/ads', [AdsController::class, 'index'])->name('settings.ads');
    Route::post('/settings/ads/update', [AdsController::class, 'update'])->name('settings.ads.update');

    // Blog Setting
    Route::get('/settings/blog', [BlogSettingController::class, 'index'])->name('settings.blog');
    Route::post('/settings/blog/update', [BlogSettingController::class, 'update'])->name('settings.blog.update');

    // languages
    Route::resource('/settings/languages', LanguageController::class);
    Route::post('/settings/languages/update_translation', [LanguageController::class, 'update_translation'])->name('languages.update_translation');
    Route::get('/settings/languages/{language}/seo', [LanguageController::class, 'show_seo'])->name('languages.show.seo');
    Route::get('/settings/languages/{language}/text', [LanguageController::class, 'text'])->name('languages.show.text');

    // SMTP Setting
    Route::get('/settings/smtp', [SmtpController::class, 'index'])->name('settings.smtp');
    Route::post('/settings/smtp/update', [SmtpController::class, 'update'])->name('settings.smtp.update');
    Route::post('/settings/check/smtp', [SmtpController::class, 'check'])->name('check.smtp');
    // Profile Settings
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::post('/profile/info/update', [ProfileController::class, 'changeInfo'])->name('settings.info.update');
    Route::post('/profile/password/update', [ProfileController::class, 'changePassword'])->name('settings.password.update');

    // API  Settings
    Route::get('/settings/api', [APIController::class, 'admin_api'])->name('settings.api');

    Route::get('/settings/css_js', [GeneralController::class, 'css_js'])->name('settings.css.js');
    Route::post('/settings/css_js/update', [GeneralController::class, 'css_js_update'])->name('settings.css.js.update');

    Route::get('/posts/checkslug', [PostController::class, 'checkSlug'])->name('posts.checkslug');
    Route::get('/categories/checkslug', [CategoryController::class, 'checkSlug'])->name('categories.checkslug');
    Route::get('/pages/checkslug', [PageController::class, 'checkSlug'])->name('pages.checkslug');
    Route::get('/posts/getcategory/{lang}', [PostController::class, 'getCategory'])->name('posts.getCategory');
    Route::post('ckeditor/image_upload', [PageController::class, 'upload'])->name('ckeditor.upload');

    Route::resource('/posts', PostController::class);
    Route::resource('/categories', CategoryController::class);
    Route::resource('/pages', PageController::class);
    Route::resource('/features', FeatureController::class);
    Route::resource('/menu', MenuController::class);

    Route::get('/clear-cache', [DashboardController::class, 'clear'])->name('clear.cache');
});

Auth::routes(['register' => false]);

Route::get('/delete', [TrashMailController::class, 'delete'])->name('delete');

Route::get('/delete/{id}', [TrashMailController::class, 'deletemessage'])->name('delete.message');

Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');

Route::post('/check_bot', [TrashMailController::class, 'check_bot'])->name('check_bot');

Route::post('/create', [TrashMailController::class, 'create'])->name('create');

Route::get('/download/{id}/{file?}', [TrashMailController::class, 'download']);

Route::post('/messages', [TrashMailController::class, 'messages'])->name('messages');

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeCookieRedirect', 'localizationRedirect', 'localeViewPath'],
], function () {
    if (ModelSettings::selectSettings('enable_blog')) {
        Route::get('/blog', [BlogController::class, 'index'])->name('blog');

        Route::get('/post/{slug}', [PostController::class, 'show'])->name('post');

        Route::get('/category/{slug}', [CategoryController::class, 'show'])->name('category');
    }

    Route::get('/', [TrashMailController::class, 'index'])->name('home');

    Route::get('/index', [TrashMailController::class, 'index'])->name('index');

    Route::get('/change', [TrashMailController::class, 'change'])->name('change');

    Route::get('/view/{id}', [TrashMailController::class, 'show'])->name('view');

    Route::get('/message/{id}', [TrashMailController::class, 'message'])->name('message');

    Route::get('/page/{slug}', [PageController::class, 'show'])->name('page');

    Route::get('/contact', [ContactController::class, 'index'])->name('contact');

    Route::get('/token/{token}', [TrashMailController::class, 'tokenToEmail']); // Only API
});
